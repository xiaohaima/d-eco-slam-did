# library imports
from nowcast_lstm.LSTM import LSTM
import numpy as np
import pandas as pd
import torch
from pandas import Series, DataFrame

import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [15, 10]

distance = [0.7, 1.1, 1.8, 2.1, 2.3, 2.6, 3, 3.1, 3.4, 3.8, 4.3, 4.6, 4.8, 5.5, 6.1]
loss = [14.1, 17.3, 17.8, 24, 23.1, 19.6, 22.3, 27.5, 26.2, 26.1, 31.3, 31.3, 36.4, 36, 43.2]
data = pd.DataFrame({'distance': distance, 'loss': loss})

area_list = ['Cameroon', 'Chad', 'Zambia', 'Turkmenistan', 'Armenia', 'Namibia', 'Kenya', 'Bhutan',
             'Ethiopia', 'Malawi', 'Angola', 'Sudan', 'Nepal', 'Fiji', 'Thailand', 'Cuba']


class AreaData:
    def __init__(self, area_name):
        self.area_name = area_name
        self.apv = {}
        self.cf = {'overall': {}, 'rural': {}, 'urban': {}}
        self.gdp = {}
        self.lf = {}
        self.inf = {}
        self.inc = {}

    def add_apv(self, df):
        for index, row in df.iterrows():
            year = str(row['Year'])
            self.apv[year] = float(row['Value'])

    def add_cf(self, df):
        for index, row in df.iterrows():
            # print(row)
            if row['Series Code'] == 'EG.CFT.ACCS.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['overall'][year] = float(row[year]) if row[year]!='..' else None
            if row['Series Code'] == 'EG.CFT.ACCS.RU.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['rural'][year] = float(row[year]) if row[year]!='..' else None
            if row['Series Code'] == 'EG.CFT.ACCS.UR.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['urban'][year] = float(row[year]) if row[year]!='..' else None

    def add_gdp(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'NY.GDP.PCAP.CD':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.gdp[year] = float(row[year]) if row[year] != '..' else None

    def add_lf(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'SL.TLF.TOTL.IN':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.lf[year] = float(row[year]) if row[year] != '..' else None

    def add_inf(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'FP.CPI.TOTL.ZG':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.inf[year] = float(row[year]) if row[year] != '..' else None

    def add_inc(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'NY.ADJ.NNTY.PC.CD':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.inc[year] = float(row[year]) if row[year] != '..' else None

    def fit_lstm_df(self):
        data = {'date': [], 'gdp': [], 'clean_fuel': [], 'income': [], 'inf': [], 'lf': []}
        data_tf = {'date': [], 'gdp': [], 'clean_fuel': [], 'income': [], 'inf': [], 'lf': []}
        for i, year in enumerate(range(2000, 2020)):
            year = str(year)
            data['date'].append(year)
            data['gdp'].append(self.gdp[year])
            data['clean_fuel'].append(self.cf['overall'][year])
            data['income'].append(self.inc[year])
            data['inf'].append(self.inf[year])
            data['lf'].append(self.lf[year])

        data_tf['date'] = data['date']
        data_tf['gdp'] = growth_rate(data['gdp'])
        data_tf['clean_fuel'] = growth_rate(data['clean_fuel'])
        data_tf['income'] = growth_rate(data['income'])
        data_tf['inf'] = growth_rate(data['inf'])
        data_tf['lf'] = growth_rate(data['lf'])
        df = DataFrame(data_tf)
        df['date'] = pd.to_datetime(df['date'])
        target_variable = "gdp"
        train_start_date = "1999-01-01"
        train_end_date = "2019-12-01"
        test_start_date = "2008-01-01"
        test_end_date = "2019-12-01"
        train = df.loc[(df.date >= train_start_date) & (df.date <= train_end_date), :].reset_index(drop=True)
        test = df.loc[(df.date >= train_start_date) & (df.date <= test_end_date), :].reset_index(
            drop=True)  # let test set data begin from training begin, data lag/time series for test sets can go back into the training data

        model = LSTM(
            data=train,
            target_variable=target_variable,
            n_timesteps=6,
            fill_na_func=np.nanmean,
            fill_ragged_edges_func=np.nanmean,
            n_models=10,
            train_episodes=100,
            batch_size=50,
            decay=0.98,
            n_hidden=10,
            n_layers=1,
            dropout=0.0,
            criterion=torch.nn.MSELoss(),
            optimizer=torch.optim.Adam,
            optimizer_parameters={"lr": 1e-2, "weight_decay": 0.0}
        )
        model.train(quiet=True)

        dates = (
            pd.date_range(test_start_date, test_end_date, freq="12MS").strftime("%Y-%m-%d").tolist()
        )

        # actual values
        actuals = list(test.loc[test.date.isin(dates), target_variable].values)
        pred_dict = []
        for date in dates:
            # the data available for this date at this artificial vintage
            # tmp_data = gen_lagged_data(metadata, test, date, lag)
            lagged_data = test.loc[test.date <= date, :].reset_index(drop=True)
            lag = -1
            for col in lagged_data.columns[1:]:
                lagged_data.loc[(len(lagged_data) + lag - 1):, col] = np.nan
            tmp_data = lagged_data
            # the predict function will give a whole dataframe, only interested in the prediction for this date
            pred = model.predict(tmp_data).loc[lambda x: x.date == date, "predictions"].values[0]
            pred_dict.append(pred)

        pd.DataFrame({
            "actuals": actuals,
            "pred_dict": pred_dict}
        ).plot()
        plt.title(self.area_name)
        plt.show()

        t = 0

    def view_data(self):
        years = np.zeros(20)
        y = np.zeros(20)
        x = np.zeros((20, 7))
        for i, year in enumerate(range(2000, 2020)):
            years[i] = year
            year = str(year)
            if not (year in self.apv and year in self.gdp and year in self.lf and year in self.inf and year in self.cf[
                'overall']):
                return False
            y[i] = self.apv[year]
            x[i, 0] = self.gdp[year]
            x[i, 1] = self.lf[year]
            x[i, 2] = self.inf[year]
            x[i, 3] = self.cf['rural'][year]
            x[i, 4] = self.cf['overall'][year]
            x[i, 5] = self.cf['urban'][year]
            x[i, 6] = self.inc[year]

        # scatter_plot(years, y, self.area_name)
        cfr = x[:, 3]
        cfo = x[:, 4]
        cfu = x[:, 5]
        gdp = x[:, 0]
        apv = y
        inc = x[:, 6]
        gdp = (gdp - np.min(gdp))/(np.max(gdp) - np.min(gdp)) *100
        apv = (apv - np.min(apv)) / (np.max(apv) - np.min(apv)) * 100
        inc = (inc - np.min(inc)) / (np.max(inc) - np.min(inc)) * 100
        if np.mean(cfr) > 80 and np.mean(cfo) > 90:
            return False

        scatter_plot_cf(years, cfr, cfo, cfu, gdp, inc,
                        self.area_name, ylim=120)


def growth_rate(data):
    growth_rates = [None]
    prev = data[0]
    for next in data[1:]:
        if not prev:
            growth_rates.append(None)
            prev = next
            continue
        if not next:
            growth_rates.append(None)
            continue
        if prev and next:
            gr = (next-prev)/prev
            growth_rates.append(gr)
            prev = next
            continue

        growth_rates.append(None)

    return growth_rates


def scatter_plot(x, y, prefix, ylim=None):
    plt.figure(figsize=(12, 7))
    # Scatter plot
    plt.scatter(x, y, marker="*", s=30, c="r", label=prefix)
    plt.xlabel("the year")
    plt.ylabel("value")
    plt.title(prefix+" Scatterplot")
    plt.legend()
    plt.show()


def scatter_plot_cf(x, cfr, cfo, cfu, gdp, inc, prefix, ylim=None):
    plt.figure(figsize=(12, 7))
    # Scatter plot
    plt.scatter(x, cfr, marker="*", s=30, c="r", label="rural")
    plt.scatter(x, cfo, marker="o", s=30, c="g", label="overall")
    plt.scatter(x, cfu, marker="x", s=30, c="b", label="urban")
    plt.scatter(x, gdp, marker=">", s=30, c="y", label="gdp")
    plt.scatter(x, inc, marker="^", s=30, c="magenta", label="inc")
    plt.xlabel("the year")
    plt.ylabel("value")
    if ylim:
        plt.ylim((0, ylim))
    plt.title(prefix+" Scatterplot")
    plt.legend()
    plt.show()


df_apv = pd.read_csv("./data/agricultural-production-value.csv", sep=",", decimal=".")
df_cf = pd.read_csv("./data/clean-fuel.csv", sep=",", decimal=".")
df_gdp = pd.read_csv("./data/GDP.csv", sep=",", decimal=".")
df_lf = pd.read_csv("./data/labor-force.csv", sep=",", decimal=".")
df_inf = pd.read_csv("./data/inflation.csv", sep=",", decimal=".")
df_inc = pd.read_csv("./data/income.csv", sep=",", decimal=".")



drop_features_apv = ['Domain Code', 'Domain', 'Area Code (FAO)', 'Element Code', 'Element', 'Item Code (FAO)',
                     'Item', 'Year Code', 'Unit', 'Flag', 'Flag Description']

drop_features_wb = ['Country Code', 'Series Name']

df_apv = df_apv.drop(drop_features_apv, axis=1)
df_cf = df_cf.drop(drop_features_wb, axis=1)
df_gdp = df_gdp.drop(drop_features_wb, axis=1)
df_lf = df_lf.drop(drop_features_wb, axis=1)
df_inf = df_inf.drop(drop_features_wb, axis=1)
df_inc = df_inc.drop(drop_features_wb, axis=1)


wb_columns = df_cf.columns.tolist()
for i, year in enumerate(range(2000, 2022)):
    old_col = str(year) + ' [YR' + str(year) + ']'
    new_col = str(year)
    if wb_columns[i+2] == old_col:
        wb_columns[i + 2] = new_col
    else:
        "wb columns name error"


df_cf.columns = wb_columns
df_gdp.columns = wb_columns
df_lf.columns = wb_columns
df_inf.columns = wb_columns
df_inc.columns = wb_columns

df_apvCD = df_apv.loc[df_apv['Area'] == "C?te d'Ivoire", 'Area'] = "Cote d'Ivoire"

# count country names
cou_cf = df_cf['Country Name'].value_counts()
cou_apv = df_apv['Area'].value_counts()

common_area = []
for i1, v1 in cou_cf.iteritems():
    for i2, v2 in cou_apv.iteritems():
        if i1 == i2:
            common_area.append(i1)

print("len common = ", len(common_area))

area_data_list = []
for area in common_area:
    df_apv_area = df_apv.loc[df_apv['Area'] == area]
    df_cf_area = df_cf.loc[df_cf['Country Name'] == area]
    df_gdp_area = df_gdp.loc[df_gdp['Country Name'] == area]
    df_lf_area = df_lf.loc[df_lf['Country Name'] == area]
    df_inf_area = df_inf.loc[df_inf['Country Name'] == area]
    df_inc_area = df_inc.loc[df_inc['Country Name'] == area]
    area_data = AreaData(area)
    area_data.add_apv(df_apv_area)
    area_data.add_cf(df_cf_area)
    area_data.add_gdp(df_gdp_area)
    area_data.add_lf(df_lf_area)
    area_data.add_inf(df_inf_area)
    area_data.add_inc(df_inc_area)
    area_data_list.append(area_data)


view_list = ['Kenya', 'Ghana', 'Mali', 'Ethiopia', 'Malawi', 'Chad',
             'Nigeria', 'Tanzania', 'Cameroun', "Cote d'Ivoire"]
for area_data in area_data_list:
    if area_data.area_name in view_list:
        area_data.fit_lstm_df()

