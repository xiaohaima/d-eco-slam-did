import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt

distance = [0.7, 1.1, 1.8, 2.1, 2.3, 2.6, 3, 3.1, 3.4, 3.8, 4.3, 4.6, 4.8, 5.5, 6.1]
loss = [14.1, 17.3, 17.8, 24, 23.1, 19.6, 22.3, 27.5, 26.2, 26.1, 31.3, 31.3, 36.4, 36, 43.2]
data = pd.DataFrame({'distance': distance, 'loss': loss})

area_list = ['Cameroon', 'Chad', 'Zambia', 'Turkmenistan', 'Armenia', 'Namibia', 'Kenya', 'Bhutan',
             'Ethiopia', 'Malawi', 'Angola', 'Sudan', 'Nepal', 'Fiji', 'Thailand', 'Cuba']


class AreaData:
    def __init__(self, area_name):
        self.area_name = area_name
        self.apv = {}
        self.cf = {'overall': {}, 'rural': {}, 'urban': {}}
        self.gdp = {}
        self.lf = {}
        self.inf = {}
        self.inc = {}

    def add_apv(self, df):
        for index, row in df.iterrows():
            year = str(row['Year'])
            self.apv[year] = float(row['Value'])

    def add_cf(self, df):
        for index, row in df.iterrows():
            # print(row)
            if row['Series Code'] == 'EG.CFT.ACCS.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['overall'][year] = float(row[year]) if row[year]!='..' else None
            if row['Series Code'] == 'EG.CFT.ACCS.RU.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['rural'][year] = float(row[year]) if row[year]!='..' else None
            if row['Series Code'] == 'EG.CFT.ACCS.UR.ZS':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.cf['urban'][year] = float(row[year]) if row[year]!='..' else None

    def add_gdp(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'NY.GDP.PCAP.CD':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.gdp[year] = float(row[year]) if row[year] != '..' else None

    def add_lf(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'SL.TLF.TOTL.IN':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.lf[year] = float(row[year]) if row[year] != '..' else None

    def add_inf(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'FP.CPI.TOTL.ZG':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.inf[year] = float(row[year]) if row[year] != '..' else None

    def add_inc(self, df):
        for index, row in df.iterrows():
            if row['Series Code'] == 'NY.ADJ.NNTY.PC.CD':
                for i, year in enumerate(range(2000, 2022)):
                    year = str(year)
                    self.inc[year] = float(row[year]) if row[year] != '..' else None

    def fit_ols_data(self, y, x, auto_fill=True):
        # y = np.zeros(19)
        # x = np.zeros((4, 19))
        for i, year in enumerate(range(2000, 2020)):
            year = str(year)
            if not (year in self.apv and year in self.gdp and year in self.lf and year in self.inf and year in self.cf['overall']):
                return False

            y[i] = self.apv[year]
            x[i, 0] = self.gdp[year]
            x[i, 1] = self.lf[year]
            x[i, 2] = self.inf[year]
            x[i, 3] = self.cf['rural'][year]

            if y[i] is None or x[i, 0] is None or x[i, 1] is None or x[i, 2] is None or x[i, 3] is None:
                # print("area have none data: ", self.area_name)
                return False

            if np.isnan(y[i]) or np.isnan(x[i, 0]) or np.isnan(x[i, 1]) or np.isnan(x[i, 2]) or np.isnan(x[i, 3]):
                # print("area have nan: ", self.area_name)
                return False

            if not auto_fill:
                continue
            y[i] = y[i] if y[i] > 1 else 1
            x[i, 0] = x[i, 0] if x[i, 0] > 1 else 1
            x[i, 1] = x[i, 1] if x[i, 1] > 1 else 1
            x[i, 2] = x[i, 2] if x[i, 2] > 1 else 1
            x[i, 3] = x[i, 3] if x[i, 3] > 1 else 1

        return True

    def view_data(self):
        years = np.zeros(20)
        y = np.zeros(20)
        x = np.zeros((20, 7))
        for i, year in enumerate(range(2000, 2020)):
            years[i] = year
            year = str(year)
            if not (year in self.apv and year in self.gdp and year in self.lf and year in self.inf and year in self.cf[
                'overall']):
                return False
            y[i] = self.apv[year]
            x[i, 0] = self.gdp[year]
            x[i, 1] = self.lf[year]
            x[i, 2] = self.inf[year]
            x[i, 3] = self.cf['rural'][year]
            x[i, 4] = self.cf['overall'][year]
            x[i, 5] = self.cf['urban'][year]
            x[i, 6] = self.inc[year]

        # scatter_plot(years, y, self.area_name)
        cfr = x[:, 3]
        cfo = x[:, 4]
        cfu = x[:, 5]
        gdp = x[:, 0]
        apv = y
        inc = x[:, 6]
        gdp = (gdp - np.min(gdp))/(np.max(gdp) - np.min(gdp)) *100
        apv = (apv - np.min(apv)) / (np.max(apv) - np.min(apv)) * 100
        inc = (inc - np.min(inc)) / (np.max(inc) - np.min(inc)) * 100
        if np.mean(cfr) > 80 and np.mean(cfo) > 90:
            return False

        scatter_plot_cf(years, cfr, cfo, cfu, gdp, inc,
                        self.area_name, ylim=120)


def scatter_plot(x, y, prefix, ylim=None):
    plt.figure(figsize=(12, 7))
    # Scatter plot
    plt.scatter(x, y, marker="*", s=30, c="r", label=prefix)
    plt.xlabel("the year")
    plt.ylabel("value")
    plt.title(prefix+" Scatterplot")
    plt.legend()
    plt.show()


def scatter_plot_cf(x, cfr, cfo, cfu, gdp, inc, prefix, ylim=None):
    plt.figure(figsize=(12, 7))
    # Scatter plot
    plt.scatter(x, cfr, marker="*", s=30, c="r", label="rural")
    plt.scatter(x, cfo, marker="o", s=30, c="g", label="overall")
    plt.scatter(x, cfu, marker="x", s=30, c="b", label="urban")
    plt.scatter(x, gdp, marker=">", s=30, c="y", label="gdp")
    plt.scatter(x, inc, marker="^", s=30, c="magenta", label="inc")
    plt.xlabel("the year")
    plt.ylabel("value")
    if ylim:
        plt.ylim((0, ylim))
    plt.title(prefix+" Scatterplot")
    plt.legend()
    plt.show()


df_apv = pd.read_csv("./data/agricultural-production-value.csv", sep=",", decimal=".")
df_cf = pd.read_csv("./data/clean-fuel.csv", sep=",", decimal=".")
df_gdp = pd.read_csv("./data/GDP.csv", sep=",", decimal=".")
df_lf = pd.read_csv("./data/labor-force.csv", sep=",", decimal=".")
df_inf = pd.read_csv("./data/inflation.csv", sep=",", decimal=".")
df_inc = pd.read_csv("./data/income.csv", sep=",", decimal=".")

# df_apv_cols = df_apv.columns.tolist()
# df_cf_cols = df_cf.columns.tolist()
# df_gdp_cols = df_gdp.columns.tolist()
# df_lf_cols = df_lf.columns.tolist()

drop_features_apv = ['Domain Code', 'Domain', 'Area Code (FAO)', 'Element Code', 'Element', 'Item Code (FAO)',
                     'Item', 'Year Code', 'Unit', 'Flag', 'Flag Description']

drop_features_wb = ['Country Code', 'Series Name']

df_apv = df_apv.drop(drop_features_apv, axis=1)
df_cf = df_cf.drop(drop_features_wb, axis=1)
df_gdp = df_gdp.drop(drop_features_wb, axis=1)
df_lf = df_lf.drop(drop_features_wb, axis=1)
df_inf = df_inf.drop(drop_features_wb, axis=1)
df_inc = df_inc.drop(drop_features_wb, axis=1)

# print(df_apv.head())
# print(df_cf.head())
# print(df_gdp.head())
# print(df_lf.head())
# print(df_inf.head())
# print(df_inc.head())

# print("cols: ", df_apv.columns.tolist())
# print("cols: ", df_cf.columns.tolist())
# print("cols: ", df_gdp.columns.tolist())
# print("cols: ", df_lf.columns.tolist())
# print("cols: ", df_inf.columns.tolist())


wb_columns = df_cf.columns.tolist()
for i, year in enumerate(range(2000, 2022)):
    old_col = str(year) + ' [YR' + str(year) + ']'
    new_col = str(year)
    if wb_columns[i+2] == old_col:
        wb_columns[i + 2] = new_col
    else:
        "wb columns name error"

df_cf.columns = wb_columns
df_gdp.columns = wb_columns
df_lf.columns = wb_columns
df_inf.columns = wb_columns
df_inc.columns = wb_columns

df_cf.dropna(axis=0, inplace=True)
df_gdp.dropna(axis=0, inplace=True)
df_lf.dropna(axis=0, inplace=True)
df_inf.dropna(axis=0, inplace=True)
df_inc.dropna(axis=0, inplace=True)

# print("cols: ", df_apv.columns.tolist())
# print("cols: ", df_cf.columns.tolist())
# print("cols: ", df_gdp.columns.tolist())
# print("cols: ", df_lf.columns.tolist())
# print("cols: ", df_inf.columns.tolist())

# print(df_apv['Area'].value_counts())
# print(df_apv['Area'].count())

# print(df_cf['Country Name'].value_counts())
# print(df_cf['Country Name'].count())

# print(df_cf['Country Name'].name)
# print(df_apv['Area'].name)

cou_cf = df_cf['Country Name'].value_counts()
# for i, v in cou_cf.iteritems():
#     print('index: ', i, 'value: ', v)

df_apvCD = df_apv.loc[df_apv['Area'] == "C?te d'Ivoire", 'Area'] = "Cote d'Ivoire"
# df_apvCD['Area'] = "Cote d'Ivoire"

cou_apv = df_apv['Area'].value_counts()
# for i, v in cou_apv.iteritems():
#     print('index: ', i, 'value: ', v)

common_area = []
for i1, v1 in cou_cf.iteritems():
    for i2, v2 in cou_apv.iteritems():
        if i1 == i2:
            common_area.append(i1)

print(common_area)
print("len common = ", len(common_area))
area_data_list = []
for area in common_area:
    df_apv_area = df_apv.loc[df_apv['Area'] == area]
    df_cf_area = df_cf.loc[df_cf['Country Name'] == area]
    df_gdp_area = df_gdp.loc[df_gdp['Country Name'] == area]
    df_lf_area = df_lf.loc[df_lf['Country Name'] == area]
    df_inf_area = df_inf.loc[df_inf['Country Name'] == area]
    df_inc_area = df_inc.loc[df_inc['Country Name'] == area]
    area_data = AreaData(area)
    area_data.add_apv(df_apv_area)
    area_data.add_cf(df_cf_area)
    area_data.add_gdp(df_gdp_area)
    area_data.add_lf(df_lf_area)
    area_data.add_inf(df_inf_area)
    area_data.add_inc(df_inc_area)
    area_data_list.append(area_data)

print("aaa")


def fit_OLS(area_data):
    y0 = np.zeros(20)
    x0 = np.zeros((20, 4))

    ret = area_data.fit_ols_data(y0, x0)

    if not ret:
        return ret

    y = np.log(y0)
    x = np.log(x0)
    X = sm.add_constant(x)
    model = sm.OLS(y, X)
    results = model.fit()
    # print("<<<<-------------------------------------------------------------------->>>>>")
    # print(results.summary())

    return ret


count_y = []
count_n = []
for area_data in area_data_list:
    ret = fit_OLS(area_data)
    if ret:
        count_y.append(area_data.area_name)
    else:
        count_n.append(area_data.area_name)

print("count_y** = ", count_y)
print("count_y = ", len(count_y))
print("count_n = ", len(count_n))

# from statsmodels.formula.api import ols
# mod = ols(formula='forecast_error ~ forecast_revision', data=df)
# fit = mod.fit(cov_type = "HAC", cov_kwds = {'maxlags':3})
# print(fit.summary())

# view_list = ['Kenya', 'Ghana', 'Mali', 'Ethiopia', 'Bhutan', 'Malawi', 'Sudan', 'Nepal', 'Fiji', 'Thailand']
view_list = ['Kenya', 'Ghana', 'Mali', 'Ethiopia', 'Bhutan', 'Malawi', 'Sudan', 'Chad',
             'Nigeria', 'Tanzania', 'Cameroun', "Cote d'Ivoire"]
for area_data in area_data_list:
    if area_data.area_name in view_list:
        area_data.view_data()

